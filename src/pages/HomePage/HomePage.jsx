import { SortPopup } from '../../components/SortPopup/SortPopup';
import { Categories } from '../../components/Categories/Categories';
import { PizzaBlock } from '../../components/PizzaBlock/PizzaBlock';
import { useDispatch, useSelector } from 'react-redux';
import { category, filters } from '../../redux/redusers/filters';
import React from 'react';
import { fetchPizzas } from '../../redux/redusers/pizzas';
import { pizzaAddBasket } from '../../redux/redusers/card';
import './HomePage.scss';


const categoryName = ['Мясные','Вегетарианские','Гриль','Острые','Закрытые'];
const sortTypePopup = [
  {name:'Популярности', type: 'rating'},
  {name:'Алфавиту', type:'name'},
  {name:'Цене',type:'price'}
  ];

export const HomePage = () => {

  const dispatch = useDispatch();

  const items = useSelector(({pizzas}) => pizzas.items );
  //const isLoaded = useSelector(({setLoaded}) =>setLoaded.isLoaded );
  const categorySelect = useSelector(({category}) => category.category );
  const sortBy = useSelector(({filters}) => filters.sortBy);
  const countPizzasInBascket = useSelector(({pizzaAddBasket}) => pizzaAddBasket.itemsInCart);
console.log(sortBy)
  React.useEffect(()=>{
    dispatch(fetchPizzas(sortBy, categorySelect));
  },[sortBy, categorySelect]);  
    
  const onSelectCategory = React.useCallback((index) => {
    dispatch(category(index));
  },[]);   

  const onSelectSortType = React.useCallback((type) => {
    dispatch(filters(type));
   
  },[]);
  
  const handleAddPizzaToCart = (obj) => {
    dispatch(pizzaAddBasket(obj))
  }
    
  
  return(
    <div className="container">
      <div className="content__top">
        <Categories  categoryType={categoryName}
        onClickCategory={onSelectCategory} activeCategory={categorySelect}/>
        <SortPopup itemsSelect={sortTypePopup}
         activeSortType={sortBy} onClickSortType={onSelectSortType}/>
      </div>
      <h2 className="content__title">Все пиццы</h2>
      <div className="content__items">
        {  
         items.map(obj => <PizzaBlock 
          onClickAddPizza={handleAddPizzaToCart}
          countThisPizzaInCard={countPizzasInBascket[obj.id] && countPizzasInBascket[obj.id].itemsForThisArr.length}
          key={obj.id} 
          {...obj} 
          />)}
        {/* : Array(10).fill(0).map((_, index) => <LoadingPage key={index}/>)} */}
       </div>
    </div>
  )
}; 