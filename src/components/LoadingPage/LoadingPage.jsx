import './LoadingPage.scss';

export const LoadingPage = () =>{
    return(
        <>
            <div className='pizza-block'>
                <div className="circle"></div>
                <div className="rectangle_string"></div>
                <div className="rectangle"></div>
                <div className="line">
                    <div className="price"></div><div className="basket"></div>
                </div>
            </div>    
        </>
    );
};