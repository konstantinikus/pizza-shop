import classNames from 'classnames';
import './Button.scss';


export const Button = ({children, className, outline, onClick}) => {
    return(
        <button 
            onClick={onClick} className={classNames('button', className, {'button--outline': outline})}>
           {children} 
        </button>
    )
}