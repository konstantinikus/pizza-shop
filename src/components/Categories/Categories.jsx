import './Categories.scss';
import React from "react";
import PropTypes, { string } from 'prop-types';


export const Categories = React.memo(({categoryType, onClickCategory, activeCategory}) => { //memo для: компонент не обновляется при обновлении 
                                                             //родителя, пропсы остаются старые

    return(
        <div className="categories">
            <ul>
                <li className={activeCategory === null ? 'active' : ''} onClick={()=>onClickCategory(null)}>Все</li>
                {categoryType && categoryType.map((item, index) =><li onClick={()=>onClickCategory(index)}
                className={activeCategory === index ? 'active' : ''}
                key={`${item}_${index}`}>{item}</li>)}
            </ul>
        </div>
    )
});

Categories.propTypes = {
    activeCategory: PropTypes.oneOf([PropTypes.number, null]),
    categoryType: PropTypes.arrayOf(string),
    
}

Categories.defaultProps = { activeCategory: null, categoryType: [] };

// class Categories extends React.Component{
//     state = {
//         activeItem: 3,
//     }

//     onSelectItems = index =>{
//         this.setState({
//             activeItem: index,
//         });

//         // this.state.activeItem = index; 
//         // this.forceUpdate(); //метод запускающий рендер
//     }
//     render(){
//         const {items} = this.props;
//         return( <div className="categories">
//                      <ul>
//                          {items.map((item, index) =><li 
//                          className={this.state.activeItem === index ? 'active' : ''} 
//                          onClick={()=>this.onSelectItems(index)} key={`${item}_${index}`}>{item}</li>)}
//                      </ul>
//                  </div>
//        )
//     }
// }




 

 