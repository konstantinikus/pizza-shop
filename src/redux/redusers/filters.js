import { createSlice } from '@reduxjs/toolkit';

export const filtersSlice = createSlice({
  name: 'filters',
  initialState:{
    category: null,
    sortBy:'rating',
  },

  reducers: {

    filters: (state, action) => {
      return{
          ...state,
          sortBy: action.payload,
      };
    },
  
    category: (state, action) => {
      return{
          ...state,
          category: action.payload,
      };
    }

  }  
});

export const {filters, category} = filtersSlice.actions;
export default filtersSlice.reducer;