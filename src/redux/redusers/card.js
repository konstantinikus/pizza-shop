

import { createSlice } from '@reduxjs/toolkit';

const getTotalPrice = arr => arr.reduce((sum, obj) => obj.price + sum,0);

export const basketSlice = createSlice({

    name: 'basket',
    initialState:{
    itemsInCart:{},
    totalPrice: 0,
    totalCount: 0,
    
    },

    reducers: {
        pizzaAddBasket: (state, action) => { 
            
            const currentPizzasItem = !state.itemsInCart[action.payload.id]? [action.payload] :
            [...state.itemsInCart[action.payload.id].itemsForThisArr, action.payload]

            const newItems = {
                ...state.itemsInCart,
                [action.payload.id]: {
                itemsForThisArr: currentPizzasItem,
                priceForThisArr: getTotalPrice(currentPizzasItem),
                },         
            };

            const allPizzasInBascket = Object.keys(newItems).reduce((sum, key) =>
            newItems[key].itemsForThisArr.length + sum, 0)
            
            const totalPriceAll = Object.keys(newItems).reduce((sum, key) =>
            newItems[key].priceForThisArr + sum, 0)
            
            return {
                ...state,
                itemsInCart: newItems,
                totalCount: allPizzasInBascket,  
                totalPrice: totalPriceAll,
            };
        },
        
        cleanBasket: (state) => { 
            return{
                ...state,
                itemsInCart:{},
                totalCount:0,
                totalPrice:0
            }
        }, 

        removeCartItem: (state, action) => {
            const newItems = {
                ...state.itemsInCart
            }

            const currentTotalPrice = newItems[action.payload].priceForThisArr;
            const currentTotalCount = newItems[action.payload].itemsForThisArr.length;
            delete newItems[action.payload]

            return{ 
                ...state,
                itemsInCart: newItems,
                totalPrice: state.totalPrice - currentTotalPrice,
                totalCount: state.totalCount - currentTotalCount,
            }
        },

        plusItemCart: (state, action) => {
            
            const newItems = [...state.itemsInCart[action.payload].itemsForThisArr,
             state.itemsInCart[action.payload].itemsForThisArr[0]]

            return {
                ...state,
                itemsInCart: {
                    ...state.itemsInCart,
                    [action.payload]:{
                        itemsForThisArr: newItems,
                        priceForThisArr: getTotalPrice(newItems),
                    },
                },
                totalCount: state.totalCount + 1,
                totalPrice: state.totalPrice + newItems[0].price
            }
        },

        minusItemCart: (state, action)=> {
            
            const oldItem = state.itemsInCart[action.payload].itemsForThisArr

        

            if(oldItem.length > 1){

                const itemsTemporaray = oldItem.slice(1)
                const newItems = {
                        ...state.itemsInCart,
                        [action.payload]: {
                        itemsForThisArr: itemsTemporaray,
                        priceForThisArr: getTotalPrice(itemsTemporaray),
                    }
                } 
            
                return {
                    ...state,
                    itemsInCart: newItems,
                    totalCount: state.totalCount - 1,
                    totalPrice: state.totalPrice - oldItem[0].price,
                }
            }
        }
    }
});



    

export const {pizzaAddBasket, cleanBasket, removeCartItem, plusItemCart, minusItemCart} = basketSlice.actions;
export default basketSlice.reducer;