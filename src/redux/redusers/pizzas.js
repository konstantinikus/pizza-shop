import { createSlice } from '@reduxjs/toolkit';
import {pizzasArray} from '../../assets/bd';

export const pizzasSlice = createSlice({

    name: 'pizzas',
    initialState:{
    items:pizzasArray,
    isLoaded: false,
    },

    reducers: {
        pizzas: (state, action) => { 
            return{
                ...state,
                items: action.payload,
                isLoaded: true,
            };
        },
        setLoaded:(state, action) => {
            return{
                ...state,
                isLoaded: action.payload,
            }
        }
    }
});


export const fetchPizzas = (sortBy, categorySelect) => dispatch => {
    let arrPizzas = pizzasArray.slice()
    let arr = [];
    
    switch (sortBy){
        case 'name':
             arrPizzas.sort((a,b) => a.name > b.name ? 1 : -1);
        break
        case 'price':
             arrPizzas.sort((a,b) => a.price > b.price ? 1 : -1);
        break
       
    } 
        
        if(categorySelect > -1 && categorySelect !== null){
            arrPizzas = arrPizzas.filter((a) => a.category === categorySelect)
        }

        if(!arrPizzas) arrPizzas = arr;
        
        dispatch(pizzas(arrPizzas));


};
    

export const {pizzas, setLoaded} = pizzasSlice.actions;
export default pizzasSlice.reducer;