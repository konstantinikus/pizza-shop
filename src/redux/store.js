import {configureStore} from '@reduxjs/toolkit';
import filters from './redusers/filters';
import category from './redusers/filters';
import pizzas from './redusers/pizzas';
import setLoaded from './redusers/pizzas';
import pizzaAddBasket from './redusers/card';
import cleanBasket from './redusers/card';
import removeCartItem from './redusers/card';
import minusItemCart from './redusers/card';
import plusItemCart from './redusers/card';


 let store = configureStore({
   reducer:{
    filters,
    category,
    pizzas,
    setLoaded,
    pizzaAddBasket,
    cleanBasket,
    removeCartItem,
    plusItemCart,
    minusItemCart,
   }
  });
  
  export default store;