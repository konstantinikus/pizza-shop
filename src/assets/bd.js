export const pizzasArray= [
    {
      id: 0,
      imageUrl: "./pizzas_pictures/0.jpg",
      name: "Пепперони Фреш с перцем",
      types: [0, 1],
      sizes: [26, 30, 40],
      price: 803,
      category: 0,
      rating: 4
    },
    {
      id: 1,
      imageUrl: "./pizzas_pictures/1.jpg",
      name: "Сырная",
      types: [0],
      sizes: [26, 40],
      price: 245,
      category: 1,
      rating: 6
    },
    {
      id: 2,
      imageUrl: "./pizzas_pictures/2.jpg",
      name: "Цыпленок барбекю",
      types: [0],
      sizes: [26, 40],
      price: 295,
      category: 1,
      rating: 4
    },
    {
      id: 3,
      imageUrl: "./pizzas_pictures/3.jpg",
      name: "Кисло-сладкий цыпленок",
      types: [1],
      sizes: [26, 30, 40],
      price: 275,
      category: 2,
      rating: 2
    },
    {
      id: 4,
      imageUrl: "./pizzas_pictures/4.jpg",
      name: "Чизбургер-пицца",
      types: [0, 1],
      sizes: [26, 30, 40],
      price: 415,
      category: 3,
      rating: 8
    },
    {
      id: 5,
      imageUrl: "./pizzas_pictures/5.jpg",
      name: "Крэйзи пепперони",
      types: [0],
      sizes: [30, 40],
      price: 580,
      category: 2,
      rating: 2
    },
    {
      id: 6,
      imageUrl: "./pizzas_pictures/6.jpg",
      name: "Пепперони",
      types: [0, 1],
      sizes: [26, 30, 40],
      price: 675,
      category: 1,
      rating: 9
    },
    {
      id: 7,
      imageUrl: "./pizzas_pictures/7.jpg",
      name: "Маргарита",
      types: [0, 1],
      sizes: [26, 30, 40],
      price: 450,
      category: 4,
      rating: 10
    },
    {
      id: 8,
      imageUrl: "./pizzas_pictures/8.jpg",
      name: "Четыре сезона",
      types: [0, 1],
      sizes: [26, 30, 40],
      price: 395,
      category: 5,
      rating: 10
    },
    {
      id: 9,
      imageUrl: "./pizzas_pictures/9.jpg",
      name: "Овощи и грибы 🌱",
      types: [0, 1],
      sizes: [26, 30, 40],
      price: 285,
      category: 5,
      rating: 7
    }
  ]
 