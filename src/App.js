import { Header } from './components/Header/Header';
import './scss/app.scss';
import { HomePage } from './pages/HomePage/HomePage';
import { Cart } from './pages/Cart/Cart';
import { Route, Routes} from 'react-router-dom';

import  React  from 'react';



function App() {

  return (
    <div className="wrapper">
      <Header/>
      <div className="content">
      <Routes>
        <Route path='/' element={<HomePage/>}/>
        <Route path='/cart' element={<Cart/>}/>
      </Routes>
      </div> 
    </div>
  );
}
 
export default App;  
  
      
  
  
  
  
  
  // CLASS COMPONENT
// class App extends React.Component {

//   componentDidMount(){
//     axios.get('http://localhost:3000/bd.json').then(({data})=>{
        
//       this.props.ActionWithPizzas(data.pizzas)
//     });
//   }
//   render() {
//     return (
//       <div className="wrapper">
//         <Header/>
//         <div className="content">
//         <Routes>
//           <Route path='/' element={<HomePage items={this.props.items}/>}/>
//           <Route path='/cart' element={<Cart/>}/>
//         </Routes>
//         </div> 
//       </div>
//     );
//   }
// }

//window.store = store;

// const mapStateToProps = (state) =>{ //объясняем что нужно вернуть состояние в пропсах
//   return{
//     items: state.pizzas.items,
//     filters: state.filters,
//   }
// };

// const mapDispatchProps = (dispatch) => {
//   return{
//     ActionWithPizzas: (items) => dispatch(setPizzas(items)), // добавляем функцию action е
//   };                                                         // в свойства обьекта и возвращаем её типа чтоб 
// };                                                           // меньше писать 

// export default connect(mapStateToProps, mapDispatchProps) (App); // благодаря этому компонент знает о store
